# LibreEnCadeau

Welcome! 


## Developing environment

LibreEnCadeau project uses strictly Eclipse, Java, GIT, Tomcat 7.

### UTF-8 compliance settings
Set UTF-8 compliance when using Tomcat server directly:
- edit workspace/Servers/Tomcat v7.0 Server at localhost-config/server.xml
- add « URIEncoding="UTF-8" » to the 8080 connetor:
  <Connector connectionTimeout="20000" port="8080" protocol="HTTP/1.1" redirectPort="8443" URIEncoding="UTF-8"/>

Set UTF-8 compliance when using Tomcat server through Apache server (so using mod_jk):
- edit server.xml (on Debian, /etc/tomcat7/server.xml)
- add « URIEncoding="UTF-8" » to the AJP connetor:
  <Connector port="8009" protocol="AJP/1.3" redirectPort="8443" URIEncoding="UTF-8"/>

### Eclipse Mars tips
To get Tomcat server with Eclipse Mars, install all JST packages.

## Build

# Authors


# License

LibreEnCadeau is released under the GNU AGPL license. 