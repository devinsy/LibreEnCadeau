/**
 * Copyright (C) 2013-2016 Christian Pierre MOMON
 * 
 * This file is part of LibreEnCadeau.
 * 
 * LibreEnCadeau is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * LibreEnCadeau is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with LibreEnCadeau. If not, see <http://www.gnu.org/licenses/>
 */
package org.april.libreencadeau;

/**
 * 
 * 
 * @author Christian P. Momon
 */
public class LibreEnCadeau
{
    private static class SingletonHolder
    {
        private static final LibreEnCadeau instance = new LibreEnCadeau();
    }

    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(LibreEnCadeau.class);

    /**
     * @throws Exception
     * 
     */
    private LibreEnCadeau()
    {
    }

    /**
     * 
     * @return
     */
    public static LibreEnCadeau instance()
    {
        return SingletonHolder.instance;
    }
}
